package Ejercicio1;

/**
 * Los Mas Altos
 */

public class Ejercicio1 {

    public static int[] numerosMasAltos(int[] numeros) {
        int i = 0;
        int maximo = 0;
        int maximo_segundo = 0;
        int[] resultado = new int[2];
        for (int numero: numeros) {
            if (numero > resultado[0]) {
                resultado[1]=resultado[0];
                resultado[0]=numero;
            }
            else if (numero > resultado[1]) {
                resultado[1]=numero;
            }
        }


        return resultado;
    }

}