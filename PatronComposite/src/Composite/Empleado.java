package Composite;

/**
 * Created by federicoaguirre on 7/3/16.
 *
 * Clase del Empleado, es el objeto simple del arbol o la hoja
 *
 */

public class Empleado implements ISueldo{
    public String nombreCompleto, cargo;
    private double sueldo;
    // demás atributos que el empleado tuviera

    @Override
    public double getSueldo() {

        return sueldo;
    }

    public Empleado(String nombreCompleto, String cargo, double sueldo) {
        setCargo(cargo);
        setNombreCompleto(nombreCompleto);
        setSueldo(sueldo);
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public void setSueldo(double sueldo) {
        this.sueldo = sueldo;
    }
}
