package Ejercicio4.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import Ejercicio4.Vendedor;


public class VendedorDAO {

    public static void saveVendedor(Vendedor vendedor) {
        Connection cn = null;
        PreparedStatement smt = null;
        try {
            String sql = "INSERT INTO PARADIGMAS.VENDEDOR(NOMBRE, APELLIDO)" + " VALUES (?,?) ";
            cn = Conexion.Conexion.getConnection();
            smt = cn.prepareStatement(sql);
            smt.setString(1, vendedor.getNombre());
            smt.setString(2, vendedor.getApellido());
            smt.executeUpdate();

            if (smt != null) {
                smt.close();
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {

                cn.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

    }

    public static void updateVendedor(Vendedor vendedor) {
        Connection cn = null;
        PreparedStatement smt = null;
        try {
            String sql = "UPDATE PARADIGMAS.VENDEDOR" + " SET NOMBRE = ? , APELLIDO = ? " + " WHERE ID_VENDEDOR = ?";
            cn = Conexion.Conexion.getConnection();
            smt = cn.prepareStatement(sql);
            smt.setString(1, vendedor.getNombre());
            smt.setString(2, vendedor.getApellido());
            smt.setInt(3, vendedor.getIdVendedor());

            smt.executeUpdate();

            if (smt != null) {
                smt.close();
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {
                cn.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

    }

    public static void deleteVendedor(int idVendedor) {
        Connection cn = null;
        PreparedStatement smt = null;
        try {
            String sql = " DELETE FROM PARADIGMAS.VENDEDOR  VEND WHERE  VEND .ID_VENDEDOR = ?";
            cn = Conexion.Conexion.getConnection();
            smt = cn.prepareStatement(sql);
            smt.setInt(1, idVendedor);
            smt.executeUpdate();
            if (smt != null) {
                smt.close();
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {

                cn.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

    }



}
