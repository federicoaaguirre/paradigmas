package Ejercicio7;

/**
 * El jueguito de los rectángulos...
 */

public class Ejercicio7 {
    public static String rectangulos(int[] a, int[] b){
       Boolean x;
       if (encajan(a,b)){
            return "Encajan sin girar";
       }else{
           b = rotarRectangulo(b);
           if (encajan(a,b)){
               return "Encajan girando";
           }
       }
        return "No encajan";
    }

    private static int[] rotarRectangulo(int[] a){
        int base = a[0];
        int altura = a[1];
        int[] arr = new int[] {altura, base};
        return arr;
    }

    private static boolean encajan(int[] a, int[] b){
        return (a[0] >= b[0]) && (a[1] >= b[1]);
    }
}
