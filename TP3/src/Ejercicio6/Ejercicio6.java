package Ejercicio6;

import java.io.*;

/**
 * Matriz traspuesta
 */

public class Ejercicio6 {
    public static Boolean matrizTranspuesta(String a, String b) throws IOException {
        BufferedReader file1 = new BufferedReader(new FileReader(a));
        BufferedWriter file2 = new BufferedWriter(new FileWriter(b));

        String line;
        Double[][] matrix = new Double[0][];


        int contador = 0;
        while ((line = file1.readLine()) != null)
        {
            String[] numbers = line.split(" ");
            for ( int i = 0 ; i < numbers.length ; i++) {
                matrix[contador][i] = Double.parseDouble(numbers[i]);
                line = line + Double.parseDouble(numbers[i]);
            }
            contador++;
            file2.write(line);
            file2.close();
        }
        return false;
    }
}
