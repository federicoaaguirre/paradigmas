package Ejercicio6;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by federicoaguirre on 5/8/16.
 */
public class Ejercicio6Test {
    @Test
    public void numerosReales() throws Exception {
        float arr[] = new float[] {2f, 5f, 3f};
        Assert.assertArrayEquals(arr, new Ejercicio6().numerosReales(1F, 5F, 3F), 0);
    }
}