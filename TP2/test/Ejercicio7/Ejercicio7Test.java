package Ejercicio7;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * El jueguito de los rectángulos...
 */
public class Ejercicio7Test {
    @Test
    public void rectangulos() throws Exception {
        int[] rectangulo1 = new int[] {4, 2};
        int[] rectangulo2 = new int[] {2, 3};
        Assert.assertEquals("No encajan", new Ejercicio7().rectangulos(rectangulo1, rectangulo2));
    }

}