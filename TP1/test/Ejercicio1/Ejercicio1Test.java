package Ejercicio1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by federicoaguirre on 4/24/16.
 */
public class Ejercicio1Test {

    @Before
    public void setUp() throws Exception {
        System.out.println("(3,4) NO es parte de la recta");
        System.out.println("(1,10) es parte de la recta");
    }

    @Test
    public void existeEnRecta() throws Exception {
        Assert.assertFalse(new Ejercicio1().existeEnRecta(3,4));
        Assert.assertTrue(new Ejercicio1().existeEnRecta(1,10));
    }
}