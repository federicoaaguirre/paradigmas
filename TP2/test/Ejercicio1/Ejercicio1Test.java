package Ejercicio1;

import org.junit.Assert;
import org.junit.Test;

/**
 * Los Mas Altos
 */

public class Ejercicio1Test {
    @Test
    public void numerosMasAltos() throws Exception {
        int[] i = new int[] {100, 2, 38, 36};
        int[] o = new int[] {100, 38};
        Assert.assertArrayEquals(o, new Ejercicio1().numerosMasAltos(i));

    }

}