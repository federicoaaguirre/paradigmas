package Ejercicio3;

/**
 * Created by federicoaguirre on 4/24/16.
 */
public class Ejercicio3 {

    public static Integer numeroFactorial(int a) {

        int resultado = a;

        for (int i=1; i<a; i++) {
            resultado = resultado * i;
        }
        return resultado;

    }
}
