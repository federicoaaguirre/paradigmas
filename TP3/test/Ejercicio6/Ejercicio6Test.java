package Ejercicio6;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Matriz traspuesta
 */

public class Ejercicio6Test {
    @Test
    public void matrizTranspuesta() throws Exception {
        Assert.assertTrue(new Ejercicio6().matrizTranspuesta("/Users/federicoaguirre/IdeaProjects/Paradigmas/TP3/Resources/matrix.txt", "/Users/federicoaguirre/IdeaProjects/Paradigmas/TP3/Resources/newMatrix.txt"));
    }

}