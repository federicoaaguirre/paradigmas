package Ejercicio4.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Ejercicio4.Venta;

public class VentaDAO {

    public static void saveVenta(Venta venta) {
        Connection cn = null;
        PreparedStatement smt = null;
        try {
            String sql = "INSERT INTO PARADIGMAS.VENTA(ID_PRODUCTO, ID_VENDEDOR,DESCRIPCION_VENTA,CANTIDAD_PRODUCTO )"
                    + "VALUES (?,?,?,?)";
            cn = Conexion.Conexion.getConnection();
            smt = cn.prepareStatement(sql);
            smt.setInt(1, venta.getIdProducto());
            smt.setInt(2, venta.getIdVendedor());
            smt.setString(3, venta.getDescripcionVenta());
            smt.setInt(4, venta.getCantidadProducto());
            smt.executeUpdate();
            if (smt != null) {
                smt.close();
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {

                cn.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

    }

    public static void updateVenta(Venta venta) {
        Connection cn = null;
        PreparedStatement smt = null;
        try {
            String sql = "UPDATE PARADIGMAS.VENTA"
                    + "SET ID_PRODUCTO = ? , ID_VENDEDOR = ? , DESCRIPCION_VENTA = ? , CANTIDAD_PRODUCTO = ?"
                    + "WHERE ID_VENTA = ?";
            cn = Conexion.Conexion.getConnection();
            smt = cn.prepareStatement(sql);
            smt.setInt(1, venta.getIdProducto());
            smt.setInt(2, venta.getIdVendedor());
            smt.setString(3, venta.getDescripcionVenta());
            smt.setInt(4, venta.getCantidadProducto());
            smt.setInt(5, venta.getIdVenta());
            smt.executeUpdate();

            if (smt != null) {
                smt.close();
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {
                cn.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

    }

    public static void deleteVenta(int idVenta) {
        Connection cn = null;
        PreparedStatement smt = null;
        try {
            String sql = " DELETE FROM PARADIGMAS.VENTA VTA WHERE VTA.ID_VENTA = ?";
            cn = Conexion.Conexion.getConnection();
            smt = cn.prepareStatement(sql);
            smt.setInt(1, idVenta);
            smt.executeUpdate();
            if (smt != null) {
                smt.close();
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {
                cn.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

    }



    //quiere saber la cantidad total de productos agrupadas por vendedor
    public static void getCantidadProductoPorVendedor() {
        String nombreVendedor = "";
        String apellidoVendedor = "";
        int cantidadProducto = 0;
        Connection cn = null;
        Statement smt = null;
        ResultSet rs = null;
        try {
            String sql = " SELECT VD.NOMBRE , VD.APELLIDO ,  SUM(VT.CANTIDAD_PRODUCTO ) Total_Vendido FROM PARADIGMAS.VENTA VT "
                    + " LEFT JOIN PARADIGMAS.VENDEDOR VD " + " ON VD.ID_VENDEDOR = VT.ID_VENDEDOR "
                    + " GROUP BY VD.NOMBRE , VD.APELLIDO " + " ORDER BY  SUM(VT.CANTIDAD_PRODUCTO ) DESC";
            cn = Conexion.Conexion.getConnection();
            smt = cn.createStatement();
            rs = smt.executeQuery(sql);
            while (rs.next()) {
                nombreVendedor = rs.getString(1);
                apellidoVendedor = rs.getString(2);
                cantidadProducto = rs.getInt(3);
                System.out.println(nombreVendedor.toUpperCase() + " " + apellidoVendedor.toUpperCase()
                        + " Total Vendido : " + cantidadProducto);

            }

            Conexion.Conexion.closeResultSet(rs);

            if (smt != null) {
                Conexion.Conexion.closeStatement(smt);
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {
                cn.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

    }


    // Quiere saber la cantidad  total vendida por producto sin importar el vendedor
    public static void getCantidadVentasPorProducto() {
        String descripcionProducto = "";
        int cantidadVendida = 0;
        Connection cn = null;
        Statement smt = null;
        ResultSet rs = null;
        try {
            String sql = " SELECT PRO.DESCRIPCION_PRODUCTO  ,  SUM(VT.CANTIDAD_PRODUCTO ) Total_Vendido FROM PARADIGMAS.VENTA VT"
                    + " LEFT JOIN PARADIGMAS.PRODUCTO PRO  "
                    + " ON PRO.ID_PRODUCTO = VT.ID_PRODUCTO "
                    + " GROUP BY PRO.DESCRIPCION_PRODUCTO "
                    + " ORDER BY  SUM(VT.CANTIDAD_PRODUCTO ) DESC ";
            cn = Conexion.Conexion.getConnection();
            smt = cn.createStatement();
            rs = smt.executeQuery(sql);
            while (rs.next()) {
                descripcionProducto = rs.getString(1);
                cantidadVendida = rs.getInt(3);
                System.out.println(descripcionProducto.toUpperCase() +" Cantidad Vendida : " + cantidadVendida);

            }

            Conexion.Conexion.closeResultSet(rs);

            if (smt != null) {
                Conexion.Conexion.closeStatement(smt);
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {
                cn.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

    }

}
