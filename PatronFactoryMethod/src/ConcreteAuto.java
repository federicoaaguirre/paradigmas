/**
 * Created by ALEJANDRO on 05/07/2016.
 */

// Definimos la implementación concreta  del Auto:
public class ConcreteAuto implements Auto {
    @Override
    public void operacion() {
        System.out.println("EL AUTO CORRE");
    }
}
