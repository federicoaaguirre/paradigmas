package ar.edu.uai.paradigms.dto.auto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Federico Donnarumma on 10/15/14.
 */
public class AutoDTO {

    private Integer id;

    private String marca;
    private int anio;

    @JsonCreator
    public AutoDTO(@JsonProperty("id") Integer id, @JsonProperty("marca") String marca, @JsonProperty("anio") int anio) {
        this.id = id;
        this.marca = marca;
        this.anio = anio;
    }

    public Integer getId() {
        return id;
    }

    public String getMarca() {
        return marca;
    }

    public int getAnio() {
        return anio;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " [id=" + id + ", marca=" + marca + ", anio=" + anio + "]";
    }

}
