package Ejercicio6;

import java.util.Arrays;

/**
 * Created by federicoaguirre on 5/7/16.
 */
public class Ejercicio6 {
    public static float[] numerosReales(float a, float b, float c){
        float menor = a;
        float medio = b;
        float mayor = c;
        for(int i = 0 ; i < 3 ; i++ ) {
            if (menor > b) {
                menor = b;
                if (menor > c) {
                    menor = c;
                }
            } else {
                if (menor > c) {
                    menor = c;
                }
            }
            if(medio > c){
                medio = c;
            }
        }
        System.out.print(menor);
        System.out.print(medio);
        System.out.print(mayor);
        float arr[] = new float[3];
        Arrays.fill(arr, menor);
        Arrays.fill(arr, medio);
        Arrays.fill(arr, mayor);
        return arr;
    }
}