package Composite;

/**
 * Created by federicoaguirre on 7/6/16.
 */
public class Rrhh{

    public static void main(String[] args) {

        // Objetos complejos

        Banco banco = new Banco();
        SectorCajas cajas = new SectorCajas();
        SectorAdministracion admin = new SectorAdministracion();

        // Objetos simples

        Empleado cajero1 = new Empleado("Federico Aguirre", "Cajero", 1500);
        Empleado cajero2 = new Empleado("Melina Montalti", "Cajero", 2000);

        Empleado administrativo1 = new Empleado("Federico Martinez", "Administrativo", 10000);
        Empleado administrativo2 = new Empleado("Javier Lopez", "Administrativo", 7654);

        cajas.agrega(cajero1);
        cajas.agrega(cajero2);

        admin.agrega(administrativo1);
        admin.agrega(administrativo2);

        banco.agrega(cajas);
        banco.agrega(admin);

        System.out.print(admin.getSueldo());

    }

}
