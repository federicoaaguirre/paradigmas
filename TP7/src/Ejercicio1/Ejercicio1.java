package Ejercicio1;
import java.util.ArrayList;
/**
 * Created by ALEJANDRO on 03/07/2016.
 */
public class Ejercicio1 {
    ArrayList arrayNoTipado = new ArrayList();
    ArrayList<String> arrayTipado = new ArrayList<String>();

    public void pruebaUno()
    {
//		arrayNoTipado.add("unElemento"); no compila porque un ArrayList tiene que estar parametrizado (tipado)
        arrayTipado.add("unElemento");
    }

    public void pruebaDos()
    {
//		String unElemento = arrayNoTipado.get(0); no compila idem arriba
        String otroElemento = arrayTipado.get(0);
    }

   /* public void pruebaTres()
    {
//		arrayNoTipado.add(3); no compila idem arriba
        arrayTipado.add(3);
    }*/

}
