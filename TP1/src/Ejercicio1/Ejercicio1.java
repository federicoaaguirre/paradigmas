package Ejercicio1;

/**
 * Created by federicoaguirre on 4/24/16.
 */

public class Ejercicio1 {

    public static Boolean existeEnRecta(int x, int y){
        Boolean resultado;
        if ( y == 7 * x + 3 ){
            resultado = true; /*"(" + x + "/" + y + ") es parte de la recta"*/
        }else{
            resultado = false; /*"(" + x + "/" + y + ") NO es parte de la recta";*/
        }
        return resultado;
    }

}