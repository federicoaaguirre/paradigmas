package Ejercicio3;

import Ejercicio3.Ejercicio3;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by federicoaguirre on 4/24/16.
 */
public class Ejercicio3Test {
    @Test
    public void numeroFactorial() throws Exception {
        Assert.assertEquals(Long.valueOf(24), Long.valueOf(new Ejercicio3().numeroFactorial(4)));
    }

}