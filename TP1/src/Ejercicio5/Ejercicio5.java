package Ejercicio5;

import com.sun.org.apache.xpath.internal.operations.Bool;

/**
 * Created by federicoaguirre on 5/7/16.
 */
public class Ejercicio5 {
    public static Integer numeroMultiplo(int a, int b){
        int contador = 1000;
        int restoa=1;
        int restob=1;

        while(contador > 0){
            restoa = contador % a ;
            restob = contador % b ;
            if(restoa == 0 && restob == 0){
                return contador;
            }
            contador=contador-1;
        }

        return contador;
    }
}
