package Genetica;

import java.util.List;
import org.junit.Test;

/**
 * Genetica
 */

public class GenTest {

    @Test
    public void test(){

        Individuo ind = new Individuo();

        Individuo ind2 = new Individuo();

        List<Gen> genesIguales = ind.genesIguales(ind2);

        for (Gen gen : genesIguales) {
            System.out.println("-------");
            System.out.println(gen.asString());
        }

        System.out.println("+++++++++++++++");
        System.out.println(genesIguales.size());
    }
}
