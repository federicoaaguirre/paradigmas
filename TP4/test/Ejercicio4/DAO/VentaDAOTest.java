package Ejercicio4.DAO;

import Ejercicio4.Venta;
import org.junit.*;

public class VentaDAOTest {

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void test() {
        // ********************************ALTA DE VENTA
        Venta venta = new Venta(1,2,"OK",4);
        VentaDAO.saveVenta(venta);

        // ********************************ACTUALIZAR DE VENTA
        // Venta venta = new Venta(1,2,"OK",4);
        //VentaDAO.updateVenta(venta);


        // ********************************ELIMINAR VENTA
        //VentaDAO.deleteVenta(1);

        // *******CANTIDAD TOTAL DE PRODUCTOS AGRUPADAS POR VENDEDOR
        //VentaDAO.getCantidadProductoPorVendedor();

        // *******CANTIDAD  TOTAL VENDIDA POR PRODUCTO SIN IMPORTAR EL VENDEDOR
        //VentaDAO.getCantidadVentasPorProducto();

    }

}
