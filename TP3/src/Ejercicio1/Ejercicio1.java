package Ejercicio1;

/**
 * Expresión aritmética
 */

public class Ejercicio1 {
    public static String expresionAritmetica(String a){
        String arr[] = a.split(" ");
        if(arr[1].equals("*")){
            float cuenta = Integer.parseInt(arr[0]) * Integer.parseInt(arr[2]);
            return arr[0]+" * "+arr[2]+" = "+String.valueOf(cuenta);
        }
        if(arr[1].equals("/")){
            float cuenta = Integer.parseInt(arr[0]) / Integer.parseInt(arr[2]);
            return arr[0]+" / "+arr[2]+" = "+String.valueOf(cuenta);
        }
        if(arr[1].equals("+")){
            float cuenta = Integer.parseInt(arr[0]) + Integer.parseInt(arr[2]);
            return arr[0]+" + "+arr[2]+" = "+String.valueOf(cuenta);
        }
        if(arr[1].equals("-")){
            float cuenta = Integer.parseInt(arr[0]) - Integer.parseInt(arr[2]);
            return arr[0]+" - "+arr[2]+" = "+String.valueOf(cuenta);
        }
        return "";
    }
}
