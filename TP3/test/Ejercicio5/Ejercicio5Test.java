package Ejercicio5;

import org.junit.Assert;
import org.junit.Test;

/**
 * Expresión balanceada
 */

public class Ejercicio5Test {
    @Test
    public void expresionBalanceada() throws Exception {
        Assert.assertTrue(new Ejercicio5().balancedParenthensies("{(2 + 2)} * (3)"));
    }

}