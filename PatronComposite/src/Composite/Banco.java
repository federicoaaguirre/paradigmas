package Composite;

/**
 * Created by federicoaguirre on 7/3/16.
 *
 * Clase de objetos complejos del composite, hereda de composite y puede contener hojas (clase simple o otras clases complejas)
 *
 */

public class Banco extends Composite {
    private int cantidadSectores;
    // acá van más atributos de la clase

    public int getCantidadSectores(){
        return cantidadSectores;
    }
    public void setCantidadSectores(int cantidadSectores){
        this.cantidadSectores = cantidadSectores;
    }
}
