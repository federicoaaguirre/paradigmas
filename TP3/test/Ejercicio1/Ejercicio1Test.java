package Ejercicio1;

import org.junit.Assert;
import org.junit.Test;

/**
 * Expresión aritmética
 */

public class Ejercicio1Test {
    @Test
    public void expresionAritmetica() throws Exception {
        Assert.assertEquals("23 * 2 = 46.0", new Ejercicio1().expresionAritmetica("23 * 2"));
    }

}