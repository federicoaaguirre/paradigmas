package Ejercicio3;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Expresiones Regulares.
 */

public class Ejercicio3 {

    public boolean validarTelefonoArgentina(String numero) {
        String regex = "";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(numero);
        return matcher.matches();
    }

    public boolean validarFecha(String numero) {
        String regex = "^[0-3][0-9]/[0-1][0-9]/(?:[0-9][0-9])?[0-9][0-9]$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(numero);
        return matcher.matches();
    }


    public boolean validarEmail(String email) {
        String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(PATTERN_EMAIL);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();

    }
}
