package Ejercicio2;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * Prolog en Java
 */

public class Ejercicio2Test {

    private Ejercicio2 ejercicio2;

    @Before
    private void setUp() throws IOException {
        this.ejercicio2 = new Ejercicio2();
    }

    @Test
    public void esHermano() {
        Assert.assertTrue(this.ejercicio2.esHermano("Mariano", "Matías"));
    }

    @Test
    public void esAbuelo() {
        Assert.assertTrue(this.ejercicio2.esAbuelo("Federico", "José"));
    }
}