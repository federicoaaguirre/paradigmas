package ar.edu.uai.paradigms.dto.auto;

/**
 * Created by federicodonnarumma on 11/4/15.
 */
public class AutoCriteriaDTO {

    private String marca;
    private Integer minAnio;
    private Integer maxAnio;

    public AutoCriteriaDTO() {}

    public AutoCriteriaDTO(String marca, Integer minAnio, Integer maxAnio) {
        this();
        this.marca = marca;
        this.minAnio = minAnio;
        this.maxAnio = maxAnio;
    }
    public Integer getMaxAnio() {
        return maxAnio;
    }

    public void setMaxAnio(Integer maxAnio) {
        this.maxAnio = maxAnio;
    }

    public Integer getMinAnio() {
        return minAnio;
    }

    public void setMinAnio(Integer minAnio) {
        this.minAnio = minAnio;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    @Override
    public String toString() {
        return "AutoCriteriaDTO{" +
                "marca='" + marca + '\'' +
                ", minAnio=" + minAnio +
                ", maxAnio=" + maxAnio +
                '}';
    }
}

