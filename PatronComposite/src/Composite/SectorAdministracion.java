package Composite;

/**
 * Created by federicoaguirre on 7/3/16.
 *
 * Clase de objetos complejos del composite, hereda de composite y puede contener hojas (clase simple o otras clases complejas)
 *
 */

public class SectorAdministracion extends Composite{
    private int cantidadCajeros;
    // acá van más atributos de la clase

    public int getCantidadCajeros(){
        return cantidadCajeros;
    }
    public void setCantidadCajeros(int cantidadCajeros){
        this.cantidadCajeros = cantidadCajeros;
    }

}
