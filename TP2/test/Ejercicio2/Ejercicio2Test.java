package Ejercicio2;

import org.junit.Assert;
import org.junit.Test;

/**
 * Desarmando Números
 */

public class Ejercicio2Test {
    @Test
    public void numeroConMasPares() throws Exception {
        int[] i = new int[] {111, 2, 384322, 422};
        int[] o = new int[] {422};
        Assert.assertArrayEquals(o, new Ejercicio2().numeroConMasPares(i));
    }

}