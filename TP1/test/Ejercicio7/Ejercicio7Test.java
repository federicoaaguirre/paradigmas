package Ejercicio7;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by federicoaguirre on 5/8/16.
 */
public class Ejercicio7Test {
    @Test
    public void timpoEnLlegar() throws Exception {
        Assert.assertEquals(Float.valueOf(91), Float.valueOf(new Ejercicio7().timpoEnLlegar(30,45)));
    }

}