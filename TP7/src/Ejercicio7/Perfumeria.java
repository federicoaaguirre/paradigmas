package Ejercicio7;

public abstract class Perfumeria implements ProductoDeVenta {
	
	private float precio;

	public float getPrecio() {
		return precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}

}
