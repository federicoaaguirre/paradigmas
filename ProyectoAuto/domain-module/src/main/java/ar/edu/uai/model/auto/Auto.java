package ar.edu.uai.model.auto;


import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "AUTO")
@Access(AccessType.FIELD)
public class Auto {

    @Id
    @GeneratedValue
    @Column(name = "AUTO_ID", unique = true, nullable = false)
    private Integer id;

    @Column(name = "MARCA", nullable = false)
    private String marca;

    @Column(name = "ANIO", nullable = false)
    private Integer anio;

    public Auto() {
    }

    public Auto(Integer id, String marca, Integer anio) {
        this.id = id;
        this.marca = marca;
        this.anio = anio;
    }

    public Integer getId() {
        return id;
    }

    public String getMarca() {
        return marca;
    }

    public int getAnio() {
        return anio;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " [id=" + id + ", marca=" + marca + ", anio=" + anio + "]";
    }
}
