package Banco;

import org.junit.Assert;
import org.junit.Test;

/**
 * Banco
 */

public class CuentaTest {

    @Test
    public void testBanco(){
        //Prueba
        Banco banco = new Banco();
        ClienteVipInterior clienteVipInterior = new ClienteVipInterior();
        Cuenta cuentaInterior = new Cuenta(clienteVipInterior);

        banco.pushCuenta(cuentaInterior);

        ClienteVipCapital clienteVipCapital = new ClienteVipCapital();
        Cuenta cuentaCapital = new Cuenta(clienteVipCapital);

        banco.pushCuenta(cuentaCapital);

        ClienteComun clienteComun = new ClienteComun();
        Cuenta cuentaComun = new Cuenta(clienteComun);

        banco.pushCuenta(cuentaComun);

        Assert.assertEquals(banco.getSaldoTotal(), Float.valueOf(0), 8);
        //System.out.println("-----------" + banco.getSaldoTotal());
        clienteComun.getCuenta(1).depositar(600);
        //System.out.println("-----------" + banco.getSaldoTotal());
        Assert.assertEquals(banco.getSaldoTotal(), Float.valueOf(594), 1);
        //System.out.println("-----------" + banco.getSaldoTotal());
        clienteVipInterior.getCuenta(1).depositar(600);
        //System.out.println("-----------" + banco.getSaldoTotal());
        Assert.assertEquals(banco.getSaldoTotal(), Float.valueOf(1194), 9);
        //System.out.println("-----------" + banco.getSaldoTotal());
    }
}
