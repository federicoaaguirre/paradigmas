package Ejercicio4;

public class Venta {

    private int idVenta;
    private int idVendedor;
    private int idProducto;
    private String descripcionVenta;
    private int cantidadProducto;

    public Venta(int idVendedor, int idProducto, String descripcionVenta, int cantidadProducto) {

        this.idVendedor = idVendedor;
        this.idProducto = idProducto;
        this.descripcionVenta = descripcionVenta;
        this.cantidadProducto = cantidadProducto;
    }

    public int getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(int idVenta) {
        this.idVenta = idVenta;
    }

    public int getIdVendedor() {
        return idVendedor;
    }

    public void setIdVendedor(int idVendedor) {
        this.idVendedor = idVendedor;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public String getDescripcionVenta() {
        return descripcionVenta;
    }

    public void setDescripcionVenta(String descripcionVenta) {
        this.descripcionVenta = descripcionVenta;
    }

    public int getCantidadProducto() {
        return cantidadProducto;
    }

    public void setCantidadProducto(int cantidadProducto) {
        this.cantidadProducto = cantidadProducto;
    }




}
