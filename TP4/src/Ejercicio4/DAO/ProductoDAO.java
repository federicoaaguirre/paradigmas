package Ejercicio4.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;



import Ejercicio4.Producto;


public class ProductoDAO {

    public static void saveProducto(Producto producto) {
        Connection cn = null;
        PreparedStatement smt = null;
        try {
            String sql = " INSERT INTO PARADIGMAS.PRODUCTO(DESCRIPCION_PRODUCTO) " + " VALUES (?,) ";
            cn = Conexion.Conexion.getConnection();
            smt = cn.prepareStatement(sql);
            smt.setString(1, producto.getDescripcionProducto());
            smt.executeUpdate();
            if (smt != null) {
                smt.close();
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {

                cn.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

    }

    public static void updateProducto(Producto producto) {
        Connection cn = null;
        PreparedStatement smt = null;
        try {
            String sql = "UPDATE PARADIGMAS.PRODUCTO " +  " SET DESCRIPCION_PRODUCTO = ? " + " WHERE ID_PRODUCTO = ? ";
            cn = Conexion.Conexion.getConnection();
            smt = cn.prepareStatement(sql);
            smt.setString(1, producto.getDescripcionProducto());
            smt.setInt(2, producto.getIdProducto());
            smt.executeUpdate();
            if (smt != null) {
                smt.close();
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {

                cn.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

    }


   /* public static void deleteProducto(int idProducto) {
        Connection cn = null;
        PreparedStatement smt = null;
        try {
            String sql = " DELETE FROM PARADIGMAS.PRODUCTO  PRO WHERE PRO.ID_PRODUCTO = ?";
            cn = Conexion.Conexion.getConnection();
            smt = cn.prepareStatement(sql);
            smt.setInt(0, idProducto);
            smt.executeUpdate();
            if (smt != null) {
                smt.close();
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {

                cn.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

    }*/




}
