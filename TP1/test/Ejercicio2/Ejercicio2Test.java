package Ejercicio2;

import Ejercicio2.Ejercicio2;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by federicoaguirre on 4/24/16.
 */
public class Ejercicio2Test {
    @Test
    public void numeroMasAlto() throws Exception {
        Assert.assertEquals(Long.valueOf(9), new Ejercicio2().numeroMasAlto(91234));
        }

}