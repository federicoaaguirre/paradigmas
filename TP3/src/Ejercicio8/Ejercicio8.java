package Ejercicio8;

/**
 * Created by federicoaguirre on 5/8/16.
 */
public class Ejercicio8 {
    public static int fac(int n) {
        int value = 0;
        if (n == 0) {
            value = 1;
        } else {
            value = n * fac(n - 1);
        }
        return value;
    }
}
