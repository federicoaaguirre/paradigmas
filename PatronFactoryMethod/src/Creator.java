/**
 * Created by ALEJANDRO on 05/07/2016.
 */
public abstract class Creator {
    // Definimos método abstracto
    public abstract Auto factoryMethod();
}
