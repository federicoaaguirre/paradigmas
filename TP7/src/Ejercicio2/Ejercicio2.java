package Ejercicio2;

import java.util.ArrayList;
import java.util.TreeSet;

/**
 * Created by ALEJANDRO on 03/07/2016.
 */
public class Ejercicio2 {

    public void set() {
        TreeSet<Integer> set = new TreeSet<>();
        set.add(1);
        set.add(2);
        set.add(3);

        System.out.println(set.size());
        System.out.println(set);

        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        System.out.println(arrayList.size());
        System.out.println(arrayList);

    }
}
