package Ejercicio6;

/**
 * Created by ALEJANDRO on 03/07/2016.
 */
public class Ticket {

    String tipoLocalidad;
    String precio;
    double monto;


    public Ticket (){}

    public Ticket(String tipoLocalidad) {
        this.tipoLocalidad = tipoLocalidad;
        this.precio = TipoLocalidadEnum.getByCodigo(tipoLocalidad.trim()).getDescripcion();
        monto= Double.parseDouble(precio);
    }

    public String getTipoLocalidad() {
        return tipoLocalidad;
    }

    public void setTipoLocalidad(String tipoLocalidad) {
        this.tipoLocalidad = tipoLocalidad;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }


    public double getMontoEnDolares()
    {
        return this.monto * 15;
    }
    public double getMontoEnEuros()
    {
        return this.monto * 16;
    }




    @Override
    public String toString() {
        return tipoLocalidad +": " + precio + "pesos" ;
    }
}
