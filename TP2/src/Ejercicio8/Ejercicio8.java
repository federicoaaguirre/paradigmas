package Ejercicio8;

/**
 * El caracol...
 */

public class Ejercicio8 {

    public float recibirValores(int profundidad, int sube, int retrocede) {
        int p = profundidad, s = sube, r = retrocede;
        float d = 0;
        d = (p - r) / (s - r);
        if ((p - r) % (s - r) != 0) {
            d = d + 1;
        }
        return d;
    }
}
