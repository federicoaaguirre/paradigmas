package Ejercicio2;

/**
 * Created by federicoaguirre on 4/24/16.
 */
public class Ejercicio2 {

    public static Long numeroMasAlto(int a) {

        long resultado = 0 ;
        int numero = a;

        while ( numero != 0 ) {
            if ( resultado < numero % 10 ) {
                resultado = ( numero % 10 );
            }
            numero /= 10 ;
        }
        return resultado;
    }
}