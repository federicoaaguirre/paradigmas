package Ejercicio9;
import java.io.*;

/**
 * El merge de archivos.
 */

public class Ejercicio9 {
    public static Boolean mergeDeArchivos(String a, String b, String c) throws IOException {

        BufferedReader file1 = new BufferedReader(new FileReader(a));
        BufferedWriter file3 = new BufferedWriter(new FileWriter(c));

        final String[] nuevaLinea = {""};

        file1.lines().forEach(line -> {
            String partsfile1[] = line.split(",");
            BufferedReader file2 = null;
            try {
                file2 = new BufferedReader(new FileReader(b));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            file2.lines().forEach(line2 -> {
                String partsfile2[] = line2.split(",");
                if(partsfile1[0].equals(partsfile2[0]))
                {
                    try {
                        file3.write(partsfile1[0]+","+partsfile1[1]+","+partsfile2[1]);
                        file3.newLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        });
        try {

            file3.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }
}
