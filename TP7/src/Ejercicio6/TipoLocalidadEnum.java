package Ejercicio6;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by ALEJANDRO on 03/07/2016.
 */
public enum TipoLocalidadEnum {
    ADELANTE("adelante", "5,"),
    MEDIO("medio", "15"),
    ATRAS("atras", "30");
    private String codigo;
    private String descripcion;

    private TipoLocalidadEnum(String codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }
    public static final List<TipoLocalidadEnum> todos = Collections.unmodifiableList(
            Arrays.asList(
                    ADELANTE,
                    MEDIO,
                    ATRAS));

    public static TipoLocalidadEnum getByCodigo(String codigo) {
        for (TipoLocalidadEnum tipoLocalidadEnum : todos) {
            if (tipoLocalidadEnum.getCodigo().equals(codigo)) {
                return tipoLocalidadEnum;
            }
        }
        return null;
    }
}
