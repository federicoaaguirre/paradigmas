package Ejercicio3;

/**
 * Piedra, Papel o Tijeras
 */

import java.util.Random;
import java.util.Scanner;

public class PPT {
    public final int Piedra = 0, Papel = 1, Tijera = 2;
    public String eleccionJugador1, eleccionJugador2;
    public int ganadorJuego;

    public void inicioPartido(String eleccionJugador1) {
        this.eleccionJugador1 = eleccionJugador1;
        this.eleccionJugador1 = this.eleccionJugador1.toUpperCase();
        setEleccionJugador2();
        inicioJuego();
        System.out.printf("%n%s", this.getGanador());
    }

    public void setEleccionJugador2() {
        Random numeroRandom = new Random();
        int eleccionPc = numeroRandom.nextInt(3);
        switch (eleccionPc) {
            case 0:
                this.eleccionJugador2 = "Piedra";
                break;
            case 1:
                this.eleccionJugador2 = "Papel";
                break;
            case 2:
                this.eleccionJugador2 = "TIJERA";
                break;
        }
        this.eleccionJugador2 = this.eleccionJugador2.toUpperCase();
        System.out.println("La eleccion del jugador 2 es: " + this.eleccionJugador2 + " y la del jugador 1: "
                + this.eleccionJugador1);
    }

    public void inicioJuego() {
        if ("PIEDRA".equalsIgnoreCase(this.eleccionJugador1)) {
            if ("PAPEL".equalsIgnoreCase(this.eleccionJugador2))
                this.ganadorJuego = 0;
            else if ("TIJERA".equalsIgnoreCase(this.eleccionJugador2))
                this.ganadorJuego = 1;
            else
                this.ganadorJuego = 2;
        } else if ("PAPEL".equalsIgnoreCase(this.eleccionJugador1)) {
            if (this.eleccionJugador2 == "PAPEL")
                this.ganadorJuego = 2;
            else if (this.eleccionJugador2 == "TIJERA")
                this.ganadorJuego = 0;
            else
                this.ganadorJuego = 1;
        } else if ("TIJERA".equalsIgnoreCase(this.eleccionJugador1)) {
            if (this.eleccionJugador2 == "TIJERA")
                this.ganadorJuego = 2;
            else if ("PIEDRA".equals(this.eleccionJugador2))
                this.ganadorJuego = 0;
            else
                this.ganadorJuego = 1;
        } else if ( !"TIJERA".equalsIgnoreCase(this.eleccionJugador1) &&  !"PAPEL".equalsIgnoreCase(this.eleccionJugador1)
                && !"PIEDRA".equalsIgnoreCase( this.eleccionJugador1 )) {
            System.out.print("El jugador no ha introducido una opcion valida");
            return;
        }
    }

    public String getGanador() {
        if (this.ganadorJuego == 0)
            return "Ganador Pc";
        else if (this.ganadorJuego == 1)
            return "Ganador Jugador";
        else if (this.ganadorJuego == 2)
            return "Empate o Tablas";
        return "Error";
    }


    public void comenzarJuego(){
        Scanner entradaTeclado = new Scanner(System.in);
        System.out.println("Introduce tu opcion (Pierda Papel o Tijera): ");
        String entradaJugador = entradaTeclado.next();
        entradaJugador = entradaJugador.toUpperCase();
        inicioPartido(entradaJugador);
        entradaTeclado.close();

    }
}
