package Ejercicio4;

import java.util.ArrayList;
import java.util.List;

/**
 * El Abuelo
 */

public class Ejercicio4 {
    public double funcionAritmetica() {

        int maximo;
        for(maximo = 1000; maximo < 10000; maximo++){
            List<Integer> numeros = digits(maximo);
            if(maximo == (Math.pow(numeros.get(3),numeros.get(2))*Math.pow(numeros.get(1),numeros.get(0)))){
                return maximo;
            }
        }
        return 6;
}

    List<Integer> digits(int i) {
        List<Integer> digits = new ArrayList<Integer>();
        while(i > 0) {
            digits.add(i % 10);
            i /= 10;
        }
        return digits;
    }

}
