package Ejercicio4;

public class Vendedor {
    private int idVendedor;
    private String nombre;
    private String apellido;

    public Vendedor(String nombre, String apellido) {

        this.nombre = nombre;
        this.apellido = apellido;
    }

    public int getIdVendedor() {
        return idVendedor;
    }

    public void setIdVendedor(int idVendedor) {
        this.idVendedor = idVendedor;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }


}
