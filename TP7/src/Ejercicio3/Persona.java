package Ejercicio3;

/**
 * Created by ALEJANDRO on 03/07/2016.
 */
public class Persona {
    private Long DNI;
    private String nombre;
    private String appellido;
    private Direccion direccion;

    public Persona(Long DNI) {
        this.DNI = DNI;
    }

    public Long getDNI() {
        return DNI;
    }

    public void setDNI(Long DNI) {
        this.DNI = DNI;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAppellido() {
        return appellido;
    }

    public void setAppellido(String appellido) {
        this.appellido = appellido;
    }

    public Direccion getDireccion() {
        return direccion;
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    @Override
    public String toString() {
        return "Persona{" + "DNI=" + DNI + ", nombre=" + nombre + ", appellido=" + appellido + '}';
    }


}
