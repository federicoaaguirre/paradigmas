package Ejercicio7;

/**
 * Created by federicoaguirre on 5/8/16.
 */
public class Ejercicio7 {
    public static float timpoEnLlegar(float a, float b) {
        float velocidad = a / 60;
        return b / velocidad;
    }
}
