/**
 * Created by ALEJANDRO on 05/07/2016.
 */

//Ahora definimos el creador concreto:
public class ConcreteCreator extends  Creator {
    @Override
    public Auto factoryMethod() {
        return new ConcreteAuto();
    }
}

    //Ejemplo de uso:
   /* public static void main(String args[]){
        Creator aCreator;
        aCreator = new ConcreteCreator();
        Auto auto = aCreator.factoryMethod();
        auto.operacion();
    }*/