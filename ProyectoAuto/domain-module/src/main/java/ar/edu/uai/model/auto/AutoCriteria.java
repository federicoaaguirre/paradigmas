package ar.edu.uai.model.auto;

/**
 * Created by federicodonnarumma on 11/4/15.
 */
public class AutoCriteria {

    private String marca;
    private Integer minAnio;
    private Integer maxAnio;

    public AutoCriteria(String marca, Integer minAnio, Integer maxAnio) {
        this.marca = marca;
        this.minAnio = minAnio;
        this.maxAnio = maxAnio;
    }
    public Integer getMaxAnio() {
        return maxAnio;
    }

    public Integer getMinAnio() {
        return minAnio;
    }

    public String getMarca() {
        return marca;
    }

    @Override
    public String toString() {
        return "AutoCriteria {" +
                "marca='" + marca + '\'' +
                ", minAnio=" + minAnio +
                ", maxAnio=" + maxAnio +
                '}';
    }
}

