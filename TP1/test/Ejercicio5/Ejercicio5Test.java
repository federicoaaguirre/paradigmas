package Ejercicio5;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by federicoaguirre on 5/7/16.
 */
public class Ejercicio5Test {
    @Test
    public void numeroMultiplo() throws Exception {
        Assert.assertEquals(Long.valueOf(980), Long.valueOf(new Ejercicio5().numeroMultiplo(5, 7)));
    }

}