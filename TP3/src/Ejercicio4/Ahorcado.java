package Ejercicio4;

import java.util.Random;
import java.util.Scanner;

/**
 * Ahorcado en Java
 */
public class Ahorcado {
    private int vidas = 4;
    String [] palabras={"amor","bebe","okey","vida","odio","coma","dios","mama","papa","tata"};
    String elegido="";
    Character[] elegidoBlur;
    String resultado="";


    public void comenzarJuego() {
        if(elegido.length()>0){
            Scanner entradaTeclado = new Scanner(System.in);
            System.out.println("Intruduce otra letra o una palabra: ");
            String entradaJugador = entradaTeclado.next();
            jugar(entradaJugador);
            entradaTeclado.close();
        }else {
            Scanner entradaTeclado = new Scanner(System.in);
            System.out.println("Intruduce un caracter o una palabra: ");
            String entradaJugador = entradaTeclado.next();
            jugar(entradaJugador);
            entradaTeclado.close();
        }
    }

    public void jugar(String c) {
        String juego = "";
        if (elegido == "") {
            elegido = elegir();
        }

        char d = '\0';
        if(c.length() > 1 && c.equals(elegido) && vidas>0){
            juego = "campeon";
        }else if(c.length() > 1 && !c.equals(elegido) && vidas>0){
            vidas--;
            comenzarJuego();
        }else if(c.length() < 2 && vidas>0){
            d = c.charAt(0);
        }

        if(vidas>0 && juego!="campeon"){
            if(existe(d)==true){
                resultado = "";
                for(int a=0;a<elegido.length(); a++) {
                    resultado = resultado + elegidoBlur[a];
                }
                System.out.print(resultado+"\n");
                if (!resultado.contains("*")) {
                    juego = "campeon";
                    System.out.print("Juego terminado usted es un campeon");
                }else {
                    comenzarJuego();
                }
            }else{
                vidas--;
                comenzarJuego();
            }
        }else if(vidas==0){
            System.out.print("GAME OVER");
        }
    }

    public String elegir(){
        Random rand = new Random();
        int aleatorio = rand.nextInt(palabras.length);
        elegido=palabras[aleatorio];
        elegidoBlur = new Character[elegido.length()];
        for(int j=0;j<elegido.length(); j++){
            elegidoBlur[j] = Character.valueOf('*');
        }
        return elegido;
    }

    public boolean existe(char car){
        boolean res=false;
        int i=0;
        while(res==false && i<elegido.length()){

            if(elegido.charAt(i)==car){
                res=true;
                elegidoBlur[i] = car;
            }else

            i++;
        }
        return res;
    }

}
