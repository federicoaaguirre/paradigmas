package Ejercicio5;

import java.io.*;

/**
 * Primos, divisores y promedio
 */

public class Ejercicio5 {

    public static double acumuladoActualPares = 0;
    public static double acumuladoActualImpares = 0;
    public static int cantidadPares = 0;
    public static int cantidadImpares = 0;

    public void generarNumero(int[] numbers) throws IOException {
        try {
            BufferedWriter primos = crearArchivo("/Users/federicoaguirre/IdeaProjects/Paradigmas/TP2/Resources/primos.txt");
            BufferedWriter divisores3000 = crearArchivo("/Users/federicoaguirre/IdeaProjects/Paradigmas/TP2/Resources/divisores_3000.txt");
            BufferedWriter promedios = crearArchivo("/Users/federicoaguirre/IdeaProjects/Paradigmas/TP2/Resources/promedios.txt");


            if(numbers.length != 0){
                for(int i=0; i < numbers.length;i++){
                    procesarNumero(numbers[i], primos, divisores3000);
                }


            }

			/*if (line != null ) {
				System.out.println(line);
				int numero = Integer.parseInt(line);
				procesarNumero(numero, primos, divisores3000);
				line = numeros.readLine();
			}*/

            escribirArchivo(promedios, "Promedio Pares: " + acumuladoActualPares / cantidadPares);
            escribirArchivo(promedios, "Promedio Impares: " + acumuladoActualImpares / cantidadImpares);
            primos.close();
            divisores3000.close();
            promedios.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private static void procesarNumero(int numero, BufferedWriter primos, BufferedWriter divisores3000) {
        // TODO Auto-generated method stub

        if (isPrimo(numero))
            escribirArchivo(primos, numero);

        if (isDivisor3000(numero))
            escribirArchivo(divisores3000, numero);

        if (isPar(numero)) {
            acumuladoActualPares += numero;
            cantidadPares++;
        } else {
            acumuladoActualImpares += numero;
            cantidadImpares++;
        }
    }

    private static boolean isPar(int numero) {
        return (numero % 2 == 0);
    }

    private static boolean isDivisor3000(int numero) {
        if (3000 % numero == 0)
            return true;
        else
            return false;
    }

    private static boolean isPrimo(int numero) {
        boolean esPrimo = true;

        for (int i = 2; (i < numero) && (esPrimo); i++) {
            if (numero % i == 0)
                esPrimo = false;
        }
        return esPrimo;
    }

    private static void escribirArchivo(BufferedWriter archivo, int numero) {
        try {
            archivo.write(String.valueOf(numero));
            archivo.newLine();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private static void escribirArchivo(BufferedWriter archivo, String string) {
        try {
            archivo.write(string);
            archivo.newLine();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private static BufferedWriter crearArchivo(String path) {
        BufferedWriter output = null;
        try {
            output = new BufferedWriter(new FileWriter(path));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return output;
    }

}
