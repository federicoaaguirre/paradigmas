package Ejercicio4;

import org.junit.Assert;
import org.junit.Test;

/**
 * El Abuelo
 */

public class Ejercicio4Test {
    @Test
    public void funcionAritmetica() throws Exception {
        Assert.assertEquals(Double.valueOf(2592), Double.valueOf(new Ejercicio4().funcionAritmetica()));
    }

}