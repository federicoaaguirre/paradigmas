package Ejercicio8;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by federicoaguirre on 5/8/16.
 */
public class Ejercicio8Test {
    @Test
    public void fac() throws Exception {
        Assert.assertEquals(3628800, new Ejercicio8().fac(10));
    }

}