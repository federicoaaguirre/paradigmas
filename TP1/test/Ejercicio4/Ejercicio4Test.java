package Ejercicio4;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by federicoaguirre on 5/7/16.
 */
public class Ejercicio4Test {
    @Test
    public void funcion() throws Exception {
        Assert.assertEquals(Long.valueOf(5), Long.valueOf(new Ejercicio4().Funcion(-3, -2)));
        Assert.assertEquals(Long.valueOf(16), Long.valueOf(new Ejercicio4().Funcion(7, 4)));
    }

}