package Composite;

/**
 * Created by federicoaguirre on 7/3/16.
 *
 * Implementada por todos los componenetes del composite
 *
 */

public interface ISueldo {
    public double getSueldo();
}