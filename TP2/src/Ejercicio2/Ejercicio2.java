package Ejercicio2;

/**
 * Desarmando Números
 */

public class Ejercicio2 {

    public static int[] numeroConMasPares(int[] numeros) {

        int[] resultado = {0};
        int pares = 0;

        for (int numero: numeros) {
            if (pares < cantidadDePares(numero)) {
                pares = cantidadDePares(numero);
                resultado[0] = numero;
            }
        }
        return resultado;
    }

    public static int cantidadDePares(Integer a) {

        int numero = a;
        int resultado = 0;
        int eval = 0;

        while ( numero != 0 ) {
            eval = ( numero % 10 );
            if ( eval % 2 == 0 ) {
                resultado++;
            }
            numero /= 10 ;
        }
        return resultado;
    }

}
